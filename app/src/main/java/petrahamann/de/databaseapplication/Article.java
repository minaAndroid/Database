package petrahamann.de.databaseapplication;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

@Entity                                     //ich bin eine Entität und ich heiße Article
public class Article {
    @NonNull                                // mehrere Annotationen möglich
    @PrimaryKey(autoGenerate = true)        //für ROOM, damit klar ist, was der Primärschlüssel ist
    private int id;     //für ROOM
    private String name;
    private double price;

    //für ROOM MUSS ein Defaultkonstruktor vorhanden sein, hat das Model einen anderen, muss man diesen
    //zusätzlich erstellen
    public Article() {
    }

    public Article(String name, double price) {
        this.name = name;
        this.price = price;
    }

    //getter und setter müssen den gleichen Namen wie die Spalten in der DB haben (DB erzeugt ROOM)
    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    //fürs testen, nicht nötig für ROOM

    @Override
    public String toString() {
        return "Article{" +
                "id=" + id +
                ", name=" + name +
                ", price=" + price + "\n";
    }
}
