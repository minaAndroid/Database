package petrahamann.de.databaseapplication;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

@Dao
public interface ArticleDAO {
    //ROOM generiert sql dafür
    @Insert
    long insert(Article article);   //damit man gleich die id eines neuen Objekts bekommt
                                    //kann auch eine Liste/ein Array als Rückgabewert sein

    @Insert
    long[] insertAll(Article... someArticles);

    @Update
    void update(Article article);

    @Delete
    void delete(Article article);

    //für Abfragen ist das nicht so "trivial", da nicht bekannt sein kann, was ich genau abfragen will...
    // dieser Methodenname wird direkt mit einer Abfrage verknüpft

    @Query("SELECT * FROM Article")
    List<Article> getArticles();

    @Query("SELECT * FROM Article")
    Article[] getArticlesArray();

    //name kommt aus Article
    @Query("SELECT * FROM Article WHERE name = :searchName")
    Article findByName(String searchName);

    @Query("SELECT COUNT(*) FROM Article")
    int getCount();

}
