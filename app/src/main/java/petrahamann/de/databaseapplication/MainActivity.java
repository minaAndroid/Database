package petrahamann.de.databaseapplication;

import android.app.Activity;
import android.arch.persistence.room.Room;
import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import java.util.List;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        ArticleDatabase database = Room.databaseBuilder(
                this,           // Referenz auf den Context
                ArticleDatabase.class,  // abstrakte DB Klasse = Typ des Rückgabewertes
                "db_article"
        ).allowMainThreadQueries().build(); //eigentlich müssten DB Zugriffe in eigenen Threads geschehen

        //DB wird jetzt tatsächlich erstellt aufgrund der ganzen Definitionen

        //alle Tabellen löschen, ohne Nachfrage und Sicherung.....
        //database.clearAllTables();

        ArticleDAO dao = database.getArticleDAO();

        Article article;
        article = new Article();
        article.setName("Handtuch");
        article.setPrice(19.99);

        Article[] articleArray = {
                new Article("Strandtasche", 7.99),
                new Article("Sonnenschirm", 12.85),
                new Article("Sitzhilfe", 21.99)
        };

        //einen Artikel hinzufügen und den PK des neuen Datensatzes loggen
        long pk = dao.insert(article);
        Log.i("new PK", String.valueOf(pk));

        //mehrere Artikel hinzufügen und Anzahl loggen
        long[] allPks = dao.insertAll(articleArray);
        Log.i("mehrere PK", String.valueOf(allPks.length));

        //alle Datensätze in Liste speichern
        List<Article> list = dao.getArticles();

        TextView textView = findViewById(R.id.db_ausgabe);
        textView.setText(list.toString());

        int anzahl = dao.getCount();
        textView.append("Anzahl Datensätze:" + anzahl + "\n");
        textView.append("Pfad zur DB:" + getDatabasePath("db_article").getAbsolutePath());
    }
}
